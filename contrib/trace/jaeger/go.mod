module gitee.com/clannad_sk/webkit/contrib/trace/jaeger/v2

go 1.18

require (
	gitee.com/clannad_sk/webkit/v2 v2.7.4
	go.opentelemetry.io/otel v1.14.0
	go.opentelemetry.io/otel/exporters/jaeger v1.14.0
	go.opentelemetry.io/otel/sdk v1.14.0
)

require (
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	go.opentelemetry.io/otel/trace v1.14.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
)

replace gitee.com/clannad_sk/webkit/v2 => ../../../
