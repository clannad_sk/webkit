// Copyright GoFrame Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package ghttp_test

import (
	"fmt"
	"testing"
	"time"

	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/net/ghttp"
	"gitee.com/clannad_sk/webkit/v2/test/gtest"
	"gitee.com/clannad_sk/webkit/v2/util/guid"
)

func Test_Context(t *testing.T) {
	s := g.Server(guid.S())
	s.Group("/", func(group *ghttp.RouterGroup) {
		group.Middleware(func(r *ghttp.Request) {
			r.SetCtxVar("traceid", 123)
			r.Middleware.Next()
		})
		group.GET("/", func(r *ghttp.Request) {
			r.Response.Write(r.GetCtxVar("traceid"))
		})
	})
	s.SetDumpRouterMap(false)
	s.Start()
	defer s.Shutdown()

	time.Sleep(100 * time.Millisecond)
	gtest.C(t, func(t *gtest.T) {
		client := g.Client()
		client.SetPrefix(fmt.Sprintf("http://127.0.0.1:%d", s.GetListenedPort()))

		t.Assert(client.GetContent(ctx, "/"), `123`)
	})
}
