// Copyright GoFrame Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package main

import (
	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/net/gsel"
	"gitee.com/clannad_sk/webkit/v2/net/gsvc"
	"gitee.com/clannad_sk/webkit/v2/os/gctx"
)

func main() {
	gsvc.SetRegistry(etcd.New(`127.0.0.1:2379`))
	gsel.SetBuilder(gsel.NewBuilderRoundRobin())

	for i := 0; i < 10; i++ {
		ctx := gctx.New()
		res := g.Client().GetContent(ctx, `http://hello.svc/`)
		g.Log().Info(ctx, res)
	}
}
