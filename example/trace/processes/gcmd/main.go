// Copyright GoFrame Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package main

import (
	"context"

	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/os/gcmd"
	"gitee.com/clannad_sk/webkit/v2/os/gctx"
	"gitee.com/clannad_sk/webkit/v2/os/gproc"
)

var (
	Main = &gcmd.Command{
		Name:  "main",
		Brief: "main process",
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			g.Log().Debug(ctx, `this is main process`)
			return gproc.ShellRun(ctx, `go run sub/sub.go`)
		},
	}
)

func main() {
	Main.Run(gctx.GetInitCtx())
}
