// Copyright GoFrame Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package main

import (
	"fmt"

	_ "gitee.com/clannad_sk/webkit/contrib/nosql/redis/v2"

	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/os/gctx"
)

func main() {
	var (
		ctx = gctx.New()
		key = "key"
	)
	_, err := g.Redis().HSet(ctx, key, g.Map{
		"id":    1,
		"name":  "john",
		"score": 100,
	})
	if err != nil {
		g.Log().Fatal(ctx, err)
	}

	// retrieve hash map
	value, err := g.Redis().HGetAll(ctx, key)
	if err != nil {
		g.Log().Fatal(ctx, err)
	}
	fmt.Println(value.Map())

	// scan to struct
	type User struct {
		Id    uint64
		Name  string
		Score float64
	}
	var user *User
	if err = value.Scan(&user); err != nil {
		g.Log().Fatal(ctx, err)
	}
	g.Dump(user)
}
