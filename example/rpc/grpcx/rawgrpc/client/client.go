// Copyright GoFrame Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package main

import (
	"fmt"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "gitee.com/clannad_sk/webkit/example/rpc/grpcx/rawgrpc/helloworld"
	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/net/gsvc"
	"gitee.com/clannad_sk/webkit/v2/os/gctx"
	"gitee.com/clannad_sk/webkit/v2/os/gfile"
)

func main() {
	grpcx.Resolver.Register(file.New(gfile.Temp("gsvc")))

	var (
		ctx     = gctx.GetInitCtx()
		service = gsvc.NewServiceWithName(`hello`)
	)
	// Set up a connection to the server.
	conn, err := grpc.Dial(
		fmt.Sprintf(`%s://%s`, gsvc.Schema, service.GetKey()),
		grpcx.Balancer.WithRandom(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		g.Log().Fatalf(ctx, "did not connect: %v", err)
	}
	defer conn.Close()

	// Send requests.
	client := pb.NewGreeterClient(conn)
	for i := 0; i < 10; i++ {
		res, err := client.SayHello(ctx, &pb.HelloRequest{Name: `GoFrame`})
		if err != nil {
			g.Log().Fatalf(ctx, "could not greet: %+v", err)
		}
		g.Log().Printf(ctx, "Greeting: %s", res.Message)
		time.Sleep(time.Second)
	}
}
