module gitee.com/clannad_sk/webkit/example

go 1.21

require (
	gitee.com/clannad_sk/webkit/contrib/config/consul/v2 v2.7.4
	gitee.com/clannad_sk/webkit/contrib/drivers/mysql/v2 v2.7.4
	gitee.com/clannad_sk/webkit/contrib/nosql/redis/v2 v2.7.4
	gitee.com/clannad_sk/webkit/v2 v2.7.4
	github.com/hashicorp/consul/api v1.24.0
	github.com/nacos-group/nacos-sdk-go/v2 v2.2.7
	github.com/polarismesh/polaris-go v1.5.5
	github.com/prometheus/client_golang v1.20.2
	go.opentelemetry.io/otel/exporters/prometheus v0.46.0
	golang.org/x/time v0.6.0
	google.golang.org/grpc v1.65.0
	google.golang.org/protobuf v1.34.2
	k8s.io/client-go v0.27.4
)

require (
	github.com/BurntSushi/toml v1.4.0 // indirect
	github.com/armon/go-metrics v0.4.1 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/clbanning/mxj/v2 v2.7.0 // indirect
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/emicklei/go-restful/v3 v3.9.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/fatih/color v1.17.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/go-logr/logr v1.4.2 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.1 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/google/gnostic v0.5.7-v3refs // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/gofuzz v1.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/grokify/html-strip-tags-go v0.1.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-hclog v1.5.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.1 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/hashicorp/go-rootcerts v1.0.2 // indirect
	github.com/hashicorp/golang-lru v1.0.2 // indirect
	github.com/hashicorp/serf v0.10.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/polarismesh/specification v1.4.1 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.55.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	go.opentelemetry.io/otel v1.29.0 // indirect
	go.opentelemetry.io/otel/metric v1.29.0 // indirect
	go.opentelemetry.io/otel/sdk v1.29.0 // indirect
	go.opentelemetry.io/otel/sdk/metric v1.24.0 // indirect
	go.opentelemetry.io/otel/trace v1.29.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d // indirect
	golang.org/x/net v0.28.0 // indirect
	golang.org/x/oauth2 v0.21.0 // indirect
	golang.org/x/sys v0.24.0 // indirect
	golang.org/x/term v0.23.0 // indirect
	golang.org/x/text v0.17.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240823204242-4ba0660f739c // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	k8s.io/api v0.27.4 // indirect
	k8s.io/apimachinery v0.27.4 // indirect
	k8s.io/klog/v2 v2.90.1 // indirect
	k8s.io/kube-openapi v0.0.0-20230501164219-8b0f38b5fd1f // indirect
	k8s.io/utils v0.0.0-20230209194617-a36077c30491 // indirect
	sigs.k8s.io/json v0.0.0-20221116044647-bc3834ca7abd // indirect
	sigs.k8s.io/structured-merge-diff/v4 v4.2.3 // indirect
	sigs.k8s.io/yaml v1.3.0 // indirect
)

replace (
	gitee.com/clannad_sk/webkit/contrib/config/apollo/v2 => ../contrib/config/apollo/
	gitee.com/clannad_sk/webkit/contrib/config/consul/v2 => ../contrib/config/consul/
	gitee.com/clannad_sk/webkit/contrib/config/kubecm/v2 => ../contrib/config/kubecm/
	gitee.com/clannad_sk/webkit/contrib/config/nacos/v2 => ../contrib/config/nacos/
	gitee.com/clannad_sk/webkit/contrib/config/polaris/v2 => ../contrib/config/polaris/
	gitee.com/clannad_sk/webkit/contrib/drivers/mysql/v2 => ../contrib/drivers/mysql/
	gitee.com/clannad_sk/webkit/contrib/metric/otelmetric/v2 => ../contrib/metric/otelmetric
	gitee.com/clannad_sk/webkit/contrib/nosql/redis/v2 => ../contrib/nosql/redis/
	gitee.com/clannad_sk/webkit/contrib/registry/etcd/v2 => ../contrib/registry/etcd/
	gitee.com/clannad_sk/webkit/contrib/registry/file/v2 => ../contrib/registry/file/
	gitee.com/clannad_sk/webkit/contrib/registry/nacos/v2 => ../contrib/registry/nacos/
	gitee.com/clannad_sk/webkit/contrib/registry/polaris/v2 => ../contrib/registry/polaris/
	gitee.com/clannad_sk/webkit/contrib/rpc/grpcx/v2 => ../contrib/rpc/grpcx/
	gitee.com/clannad_sk/webkit/contrib/trace/otlpgrpc/v2 => ../contrib/trace/otlpgrpc
	gitee.com/clannad_sk/webkit/contrib/trace/otlphttp/v2 => ../contrib/trace/otlphttp
	gitee.com/clannad_sk/webkit/v2 => ../
)
