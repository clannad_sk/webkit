// Copyright GoFrame Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package main

import (
	_ "gitee.com/clannad_sk/webkit/example/pack/packed"

	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/i18n/gi18n"
	"gitee.com/clannad_sk/webkit/v2/net/ghttp"
	"gitee.com/clannad_sk/webkit/v2/os/gres"
)

func main() {
	gres.Dump()

	s := g.Server()
	s.SetPort(8199)
	s.SetServerRoot("resource/public")
	s.BindHandler("/i18n", func(r *ghttp.Request) {
		var (
			lang    = r.Get("lang", "zh-CN").String()
			ctx     = gi18n.WithLanguage(r.Context(), lang)
			content string
		)
		content = g.I18n().T(ctx, `{#hello} {#world}!`)
		r.Response.Write(content)
	})
	s.SetErrorLogEnabled(true)
	s.SetAccessLogEnabled(true)
	s.Run()
	// gf pack resource packed/packed.go
	// http://127.0.0.1:8199/index.html
}
