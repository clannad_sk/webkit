// Copyright GoFrame gf Author(https://goframe.org). All Rights Reserved.
//
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file,
// You can obtain one at https://github.com/gogf/gf.

package cmd

import (
	_ "gitee.com/clannad_sk/webkit/contrib/drivers/clickhouse/v2"
	// _ "gitee.com/clannad_sk/webkit/contrib/drivers/dm/v2" // precompilation does not support certain target platforms.
	_ "gitee.com/clannad_sk/webkit/contrib/drivers/mssql/v2"
	_ "gitee.com/clannad_sk/webkit/contrib/drivers/mysql/v2"
	_ "gitee.com/clannad_sk/webkit/contrib/drivers/oracle/v2"
	_ "gitee.com/clannad_sk/webkit/contrib/drivers/pgsql/v2"
	_ "gitee.com/clannad_sk/webkit/contrib/drivers/sqlite/v2"

	"gitee.com/clannad_sk/webkit/cmd/gf/v2/internal/cmd/gendao"
)

type (
	cGenDao = gendao.CGenDao
)
