package dict

import (
	"context"

	"gitee.com/clannad_sk/webkit/v2/errors/gcode"
	"gitee.com/clannad_sk/webkit/v2/errors/gerror"

	"gitee.com/clannad_sk/webkit/cmd/gf/v2/internal/cmd/testdata/genctrl-merge/add_new_ctrl/api/dict/v1"
)

func (c *ControllerV1) DictTypeAddPage(ctx context.Context, req *v1.DictTypeAddPageReq) (res *v1.DictTypeAddPageRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
