package article

import (
	"context"

	"gitee.com/clannad_sk/webkit/v2/errors/gcode"
	"gitee.com/clannad_sk/webkit/v2/errors/gerror"

	"gitee.com/clannad_sk/webkit/cmd/gf/v2/internal/cmd/testdata/genctrl/api/article/v2"
)

func (c *ControllerV2) Update(ctx context.Context, req *v2.UpdateReq) (res *v2.UpdateRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
