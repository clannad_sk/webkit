package testdata

import (
	"gitee.com/clannad_sk/webkit/v2/frame/g"
	"gitee.com/clannad_sk/webkit/v2/net/ghttp"
	"gitee.com/clannad_sk/webkit/v2/util/guid"
	"testing"
)

func Test_Router_Hook_Multi(t *testing.T) {
	s := g.Server(guid.S())
	s.BindHandler("/multi-hook", func(r *ghttp.Request) {
		r.Response.Write("show")
	})

	s.BindHookHandlerByMap("/multi-hook", map[string]ghttp.HandlerFunc{
		ghttp.HookBeforeServe: func(r *ghttp.Request) {
			r.Response.Write("1")
		},
	})
	s.BindHookHandlerByMap("/multi-hook/{id}", map[string]ghttp.HandlerFunc{
		ghttp.HookBeforeServe: func(r *ghttp.Request) {
			r.Response.Write("2")
		},
	})
}
