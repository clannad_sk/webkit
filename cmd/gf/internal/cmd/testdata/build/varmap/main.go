package main

import (
	"fmt"

	"gitee.com/clannad_sk/webkit/v2/os/gbuild"
)

func main() {
	for k, v := range gbuild.Data() {
		fmt.Printf("%s: %v\n", k, v)
	}
}
