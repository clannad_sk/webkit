module gitee.com/clannad_sk/webkit/cmd/gf/cmd/gf/testdata/vardump/v2

go 1.18

require gitee.com/clannad_sk/webkit/v2 v2.7.4

require (
	go.opentelemetry.io/otel v1.14.0 // indirect
	go.opentelemetry.io/otel/trace v1.14.0 // indirect
)

replace gitee.com/clannad_sk/webkit/v2 => ../../../../../../../
